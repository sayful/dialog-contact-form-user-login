<?php

use DialogContactForm\Abstracts\Template;
use DialogContactForm\Supports\Utils;

class UserLoginForm extends Template {

	public function __construct() {
		$this->priority    = 210;
		$this->id          = 'user_login';
		$this->title       = __( 'User Login', 'dialog-contact-form' );
		$this->description = __( 'Login a WordPress user.', 'dialog-contact-form' );
	}

	/**
	 * Form fields
	 *
	 * @return array
	 */
	protected function formFields() {
		return array(
			array(
				'field_type'     => 'text',
				'field_title'    => __( 'Email or Username', 'dialog-contact-form' ),
				'field_id'       => 'user_login',
				'field_name'     => 'user_login',
				'required_field' => 'on',
				'field_width'    => 'is-12',
				'autocomplete'   => 'username',
			),
			array(
				'field_type'     => 'password',
				'field_title'    => __( 'Password', 'dialog-contact-form' ),
				'field_id'       => 'user_password',
				'field_name'     => 'user_password',
				'required_field' => 'on',
				'field_width'    => 'is-12',
				'autocomplete'   => 'current-password',
			),
			array(
				'field_type'         => 'acceptance',
				'field_title'        => __( 'Remember Me', 'dialog-contact-form' ),
				'acceptance_text'    => __( 'Remember Me', 'dialog-contact-form' ),
				'field_id'           => 'remember',
				'field_name'         => 'remember',
				'field_width'        => 'is-12',
				'checked_by_default' => 'on',
			),
		);
	}

	/**
	 * Form settings
	 *
	 * @return array
	 */
	protected function formSettings() {
		return array(
			'labelPosition' => 'both',
			'btnLabel'      => esc_html__( 'Login', 'dialog-contact-form' ),
			'btnAlign'      => 'left',
			'reset_form'    => 'yes',
			'recaptcha'     => 'no',
		);
	}

	/**
	 * Form actions
	 *
	 * @return array
	 */
	protected function formActions() {
		return array(
			'user_login'      => array(
				'user_login'    => 'user_login',
				'user_password' => 'user_password',
				'remember'      => 'remember',
			),
			'success_message' => array(
				'message' => __( 'Welcome back.', 'dialog-contact-form' ),
			),
			'redirect'        => array(
				'redirect_to' => 'url',
				'url'         => site_url(),
			),
		);
	}

	/**
	 * Form validation messages
	 *
	 * @return array
	 */
	protected function formValidationMessages() {
		return array(
			'mail_sent_ng'     => __( 'There was an error trying to submit the form. Please try again later.',
				'dialog-contact-form' ),
			'validation_error' => Utils::get_option( 'validation_error' ),
		);
	}
}
