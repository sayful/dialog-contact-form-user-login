<?php

use DialogContactForm\Abstracts\Action;
use DialogContactForm\Supports\Config;
use DialogContactForm\Supports\Utils;

class UserLogin extends Action {

	/**
	 * UserRegistration constructor.
	 */
	public function __construct() {
		$this->priority   = 20;
		$this->id         = 'user_login';
		$this->title      = __( 'User Login', 'dialog-contact-form' );
		$this->meta_group = 'user_login';
		$this->meta_key   = '_action_user_login';
		$this->settings   = array_merge( $this->settings, $this->settings() );
	}

	/**
	 * Process current action
	 *
	 * @param Config $config Contact form configurations
	 * @param array $data User submitted sanitized data
	 *
	 * @return mixed
	 */
	public static function process( $config, $data ) {
		// User already login
		if ( is_user_logged_in() ) {
			if ( self::isAjax() ) {
				wp_send_json( array(
					'error_id' => 'unknown_error',
					'message'  => __( 'You are already logged in.', 'dialog-contact-form' ),
				), 422 );
			}

			return false;
		}

		$errors = array();

		$default_message    = __( 'No user found with this information.', 'dialog-contact-form' );
		$invalid_user_login = Utils::get_option( 'invalid_user_login', $default_message );

		$action_settings = get_post_meta( $config->getFormId(), '_action_user_login', true );

		$user_login    = isset( $data[ $action_settings['user_login'] ] ) ? $data[ $action_settings['user_login'] ] : null;
		$user_password = isset( $data[ $action_settings['user_password'] ] ) ? $data[ $action_settings['user_password'] ] : null;
		$remember      = isset( $data[ $action_settings['remember'] ] ) && 'on' == $action_settings[ $data['remember'] ] ? true : false;

		if ( ! ( username_exists( $user_login ) || email_exists( $user_login ) ) ) {
			$errors[ $action_settings['user_login'] ][] = $invalid_user_login;

			if ( self::isAjax() ) {
				wp_send_json( array(
					'status'     => 'fail',
					'message'    => $config->getValidationErrorMessage(),
					'validation' => $errors,
				), 422 );
			}

			$GLOBALS['_dcf_errors']           = $errors;
			$GLOBALS['_dcf_validation_error'] = $config->getValidationErrorMessage();

			return false;
		}

		$credentials = array(
			'user_login'    => $user_login,
			'user_password' => $user_password,
			'remember'      => $remember,
		);

		$user = wp_signon( $credentials, false );

		if ( is_wp_error( $user ) ) {
			if ( self::isAjax() ) {
				wp_send_json( array(
					'error_id' => 'unknown_error',
					'message'  => $user->get_error_message(),
				), 422 );
			}

			return false;
		}

		wp_set_current_user( $user->ID, $user->user_login );
		wp_set_auth_cookie( $user->ID, $remember );

		return true;
	}

	/**
	 * Action settings
	 *
	 * @return array
	 */
	private function settings() {
		$config          = Config::init();
		$placeholder     = array( '' => __( '-- No Value --', 'dialog-contact-form' ) );
		$text_fields     = $placeholder;
		$password_fields = $placeholder;
		$remember_fields = $placeholder;
		foreach ( $config->getFormFields() as $field ) {
			if ( empty( $field['field_type'] ) ) {
				continue;
			}
			if ( in_array( $field['field_type'], array( 'text', 'email' ) ) ) {
				$text_fields[ $field['field_id'] ] = $field['field_title'];
			} elseif ( 'password' === $field['field_type'] ) {
				$password_fields[ $field['field_id'] ] = $field['field_title'];
			} elseif ( 'acceptance' === $field['field_type'] ) {
				$remember_fields[ $field['field_id'] ] = $field['field_title'];
			}
		}

		return array(
			'field_mapping' => array(
				'type'  => 'section',
				'label' => __( 'Field Mapping', 'dialog-contact-form' ),
			),
			'user_login'    => array(
				'type'     => 'select',
				'id'       => 'user_login',
				'group'    => $this->meta_group,
				'meta_key' => $this->meta_key,
				'label'    => __( 'Username or Email', 'dialog-contact-form' ),
				'options'  => $text_fields,
			),
			'user_password' => array(
				'type'     => 'select',
				'id'       => 'user_password',
				'group'    => $this->meta_group,
				'meta_key' => $this->meta_key,
				'label'    => __( 'Password', 'dialog-contact-form' ),
				'options'  => $password_fields,
			),
			'remember'      => array(
				'type'     => 'select',
				'id'       => 'remember',
				'group'    => $this->meta_group,
				'meta_key' => $this->meta_key,
				'label'    => __( 'Password', 'dialog-contact-form' ),
				'options'  => $remember_fields,
			),
		);
	}

	/**
	 * Check if it is an ajax request
	 *
	 * @return bool
	 */
	private static function isAjax() {
		return defined( 'DOING_AJAX' ) && DOING_AJAX;
	}
}
