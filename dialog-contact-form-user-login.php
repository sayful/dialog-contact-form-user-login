<?php
/**
 * Plugin Name: Dialog Contact Form - User Login
 * Plugin URI: https://bitbucket.org/sayful/dialog-contact-form-user-login/
 * Description: User Login add-on for Dialog Contact Form WordPress plugin.
 * Version: 1.0.0
 * Author: Sayful Islam
 * Author URI: https://sayfulislam.com
 * Requires at least: 4.4
 * Tested up to: 4.9
 * Text Domain: dialog-contact-form-user-login
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.txt
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Dialog_Contact_Form_User_Login' ) ) {

	class Dialog_Contact_Form_User_Login {
		/**
		 * The instance of the class
		 *
		 * @var self
		 */
		private static $instance;

		/**
		 * Ensures only one instance of the class is loaded or can be loaded.
		 *
		 * @return self
		 */
		public static function init() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();

				add_action( 'dialog_contact_form/actions', array( self::$instance, 'add_action' ) );
				add_action( 'dialog_contact_form/templates', array( self::$instance, 'add_template' ) );
				add_filter( 'dialog_contact_form/settings/fields', array( self::$instance, 'add_fields_settings' ) );
			}

			return self::$instance;
		}

		/**
		 * Add our custom action to action manager
		 *
		 * @param \DialogContactForm\Collections\Actions $actions
		 */
		public function add_action( $actions ) {
			include_once 'includes/Actions/UserLogin.php';
			$actions->set( 'user_login', 'UserLogin' );
		}

		/**
		 * Add our custom template to template manager
		 *
		 * @param \DialogContactForm\Collections\Templates $templates
		 */
		public function add_template( $templates ) {
			include_once 'includes/Templates/UserLoginForm.php';

			$templates->set( 'user_login', 'UserLoginForm' );
		}

		/**
		 * @param array $fields
		 *
		 * @return array
		 */
		public function add_fields_settings( $fields ) {
			$fields[] = array(
				'id'       => 'invalid_user_login',
				'type'     => 'textarea',
				'rows'     => 2,
				'name'     => __( 'Invalid user login', 'dialog-contact-form' ),
				'std'      => __( 'No user found with this information.', 'dialog-contact-form' ),
				'section'  => 'dcf_field_message_section',
				'priority' => 200,
			);

			return $fields;
		}
	}
}

Dialog_Contact_Form_User_Login::init();
